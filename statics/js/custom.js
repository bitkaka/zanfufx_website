/* 打开链接 */
function openlink(url) {
	window.location = url;
}
/* 打开移动端导航 */
function openNav() {
	$("#nav-layer").show();
	$(".nav-menu").animate({left: '0%'}, 300);
}
/* 关闭移动端导航 */
function closeNav() {
	$(".nav-menu").animate({left: '-80%'}, 300, function() {
		$("#nav-layer").hide();
	});
}

function switchLang(src, to) {
	if(!src && !to)
	{
		return;
	}
	var pathname = window.location.href;
	if(pathname.indexOf("/html/") == -1 )
    {
		location.href = pathname.replace("/" + src, "/" + to);
	}
	else
	{
		pathname = pathname.replace("_" + src, "");
		if(to === "cn")
		{
			to = "";
		}
		else
		{
			to = "_" + to;
		}
		var lastchar = pathname.substr(pathname.length-1,1);
		if(lastchar == "/")
		{
			location.href = pathname + "index" + to + ".html";
		}
		if(lastchar == "#")
		{
			pathname = pathname.replace("/#","/index" + to + ".html");
			location.href = pathname;
		}
		if(pathname.indexOf(".html") > 0)
		{
			if(to != "" && pathname.indexOf(to) > 0)
			{
				location.href = pathname;
			}
			else
			{
				pathname = pathname.replace(".html", to + ".html");
				location.href = pathname;
			}
		}
	}
}